public class Course {
	
	private String name;										//course name
	private int number;											//course number

	public Course(String name, int number) {
		if(name==null)																	//checking name is not null
			throw new IllegalArgumentException("Illegal Argument Exception");			
		for (int i=0; i<name.length();i=i+1){											//going over all the chars in the string name
			if((('A'>name.charAt(i)||name.charAt(i)>'Z')&&('a'>name.charAt(i)||name.charAt(i)>'z')&&('0'>name.charAt(i)||name.charAt(i)>'9'))&&name.charAt(i)!=' ')			//making sure name has only legal chars
				throw new IllegalArgumentException("Illegal Argument Exception");
		}
		if(number<=0)																	//making sure course number is positive and not 0
			throw new IllegalArgumentException("Illegal Argument Exception");	
		this.name=name;
		this.number=number;
	}

	public String getName(){															//returns course name
		return this.name;
	}

	public int getCourseNumber(){														//returns course number
		return this.number;
	}

	public String toString(){															//returns course info
		return "cours: "+this.name+" number: "+this.number ;
	}

	public boolean isEqualTo(Course other){												//returns true if course number is equal to the other course number
		
		return this.number==other.getCourseNumber();
		
	}


}
