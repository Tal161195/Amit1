

public class Student {
	private static int maxNumber=10;								//max number of courses student is allowed to be registered to
	private String name;											//student name
	private int id;													//student ID
	private Course cList[]= new Course[maxNumber];					//courses list for the student
	private int NumOfCourses;										//number of courses student is registered to 

	public Student(String name,int id){
		if(name==null||name=="")																	//checking name is not null
			throw new IllegalArgumentException("Illegal Argument Exception");			
		for (int i=0; i<name.length();i=i+1){											//going over all the chars in the string name
			if((('A'>name.charAt(i)||name.charAt(i)>'Z')&&('a'>name.charAt(i)||name.charAt(i)>'z')&&name.charAt(i)!=' '))			//making sure name has only legal chars
				throw new IllegalArgumentException("Illegal Argument Exception");
		}
		if(id<=0)																		//making sure ID number is positive and not 0
			throw new IllegalArgumentException("Illegal Argument Exception");	
		this.name=name;
		this.id=id;
	}

	public String getName(){															//returns student name
		return this.name;
	}

	public int getID(){																	//returns student ID
		return this.id;
	}
	
	public boolean registerTo(Course course){																		//j will be the index in array cList
		boolean output;
		if(NumOfCourses>=maxNumber||isRegisteredTo(course)==true)						//checks if the student is registers to less courses then the max number and and that the student is not already registered to that course
			output=false;																//if one of the above happens returns false
		else{
			cList[NumOfCourses]=course;																//puts new course in spot j in the array
			this.NumOfCourses=this.NumOfCourses+1;											//updates the number of courses the student takes
			output= true;																//if not returns false
		}
		return output;
	}

	public boolean isRegisteredTo(Course course){
		boolean rashum=false;
		for(int i=0; i<NumOfCourses&&!rashum;i=i+1){												//Reviews all the courses the student is registered to
			if(this.cList[i].getCourseNumber()==course.getCourseNumber())					//returns true if the student is registered to the course
				rashum= true;
		}	
		return rashum;												//returns true because we add the new course
	}

	
	public String toString(){
		return "student: "+ this.name+" ID number: "+this.id+" is registerd to: "+this.NumOfCourses+" courses" ;  //returns student info
	}
	public boolean isEqualTo(Student other){
		return  this.id==other.getID();													//returns true if ID number is equal to the other ID number
	}
}
