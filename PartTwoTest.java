import java.util.Arrays;

public class PartTwoTest {
	public static void main(String[] args){

		//=========== test course builder =========== 
		Course newCourse1 = new Course("a6Abc", 1); // should work
		//	Course newCourse2 = new Course("a6Ab_c", 1); // should throw an  IllegalArgumentException
		//	Course newCourse2 = new Course("ac", 0); // should throw an  IllegalArgumentException

		//=========== test course getters =========== 
		String name1 = "testName";
		int num1 = 10;
		Course newCourse2 = new Course(name1, num1); 
		String name2 = newCourse2.getName();
		int num2 = newCourse2.getCourseNumber();
		System.out.println("Course:  getName is working is " + name2.equals(name1));
		System.out.println("Course:  getCourseNumber is working is " + (num2 == num1) );

		//=========== test course toString =========== 
		String name3 = "testToString";
		int num3 = 13;
		Course newCourse3 = new Course(name3, num3);
		System.out.println(newCourse3.toString());// should print some description of the course


		//=========== test course isEqualTo =========== 
		int num10 = 10;
		Course newCourse13 = new Course("abc", num10); 
		Course newCourse14 = new Course("vb", num10);
		Course newCourse15 = new Course("vb", num10+1);

		System.out.println("Course:  isEqualTo is working is " + newCourse13.isEqualTo(newCourse14));
		System.out.println("Course:  isEqualTo is working is " + !newCourse13.isEqualTo(newCourse15));

		//==============================================
		//==============================================
		System.out.println("");

		//=========== test student builder =========== 
		Student student1 = new Student("abc", 1); // should work
		//  Student student2 = new Student("a6bc", 1); // should throw an  IllegalArgumentException
		//	Student student3 = new Student("a6Ab_c", 1); // should throw an  IllegalArgumentException
		//	Student student4 = new Student("ac", 0); // should throw an  IllegalArgumentException



		//=========== test student getters =========== 
		String name21 = "testName";
		int num21 = 10;
		Student newStudent21 = new Student(name21, num21); 
		String name22 = newStudent21.getName();
		int num22 = newStudent21.getID();
		System.out.println("Student:  getName is working is " + name22.equals(name21));
		System.out.println("Student:  getID is working is " + (num22 == num21) );



		//=========== test student isEqualTo =========== 
		int num30 = 10;
		Student newStudent13 = new Student("abc", num30); 
		Student newStudent14 = new Student("vb", num30);
		Student newStudent15 = new Student("vb", num30+1);

		System.out.println("Student:  isEqualTo is working is " + newStudent13.isEqualTo(newStudent14));
		System.out.println("Student:  isEqualTo is working is " + !newStudent13.isEqualTo(newStudent15));



		//=========== test student isRegesteredTo =========== 
		// note: in order to test this function properly, RegistrationSystem 
		// must first be implemented. 
		Student shay = new Student("shay",1);
		Course algo = new Course("algo",1);
		RegistrationSystem sys = new RegistrationSystem();

		System.out.println("student isRegesteredTo:   adding course was " + sys.addCourse(algo));
		System.out.println("student isRegesteredTo:   adding student was " + sys.addStudent(shay));
		System.out.println("student isRegesteredTo:   registering stdent to course was " + sys.register(shay, algo));
		System.out.println("student isRegesteredTo:   isRegisteredTo is working is " + shay.isRegisteredTo(algo));

		//=========== test course toString =========== 
		String name20 = "testToString";
		int num20 = 13;
		Student newStudent20 = new Student(name20, num20);
		System.out.println(newStudent20.toString());// should print some description of the student

		//==============================================
		//==============================================
		System.out.println("");

		//=========== test RegistrationSystem builder =========== 

		RegistrationSystem sys10 = new RegistrationSystem(); // should work

		//=========== test RegistrationSystem addStudent =========== 

		RegistrationSystem sys100 = new RegistrationSystem(); 
		Student student100 = new Student("regTest", 4);
		System.out.println("RegistrationSystem:   addStudent is working is " + sys100.addStudent(student100));

		boolean[] addingStudents = new boolean[500];
		RegistrationSystem addMax = new RegistrationSystem();
		for(int id = 0; id<500 ; id = id+1){
			Student toReg = new Student("regTest", id+1);
			addingStudents[id] = addMax.addStudent(toReg);
			if (addingStudents[id] == false){
				System.out.println("RegistrationSystem:   addStudent is not adding 500 students correctly");
			}
		}
		Student shouldFail = new Student("shouldFail", 501);
		System.out.println("RegistrationSystem:   addStudent added only 500 students is " + !addMax.addStudent(shouldFail));


		//=========== test RegistrationSystem addCourse =========== 

		RegistrationSystem sys200 = new RegistrationSystem(); 
		Course course100 = new Course("regTest", 4);
		System.out.println("RegistrationSystem:   addCourse is working is " + sys200.addCourse(course100));

		boolean[] addingCourses = new boolean[500];
		RegistrationSystem addMaxCourse = new RegistrationSystem();
		for(int id = 0; id<500 ; id = id+1){
			Course toReg = new Course("regTest", id+1);
			addingCourses[id] = addMaxCourse.addCourse(toReg);
			if (addingCourses[id] == false){
				System.out.println("RegistrationSystem:   addCourse is not adding 500 courses correctly");
			}
		}
		Course shouldFailCourse = new Course("shouldFail", 501);
		System.out.println("RegistrationSystem:   addCourse added only 500 courses is " + !addMaxCourse.addCourse(shouldFailCourse));




		//=========== test RegistrationSystem register =========== 
		RegistrationSystem sys210 = new RegistrationSystem(); 
		Course course101 = new Course("regTest", 4);
		Student student101 = new Student("regTest", 4);
		sys210.addCourse(course101);
		sys210.addStudent(student101);
		System.out.println("RegistrationSystem:   register is working is " + sys210.register(student101, course101));
		Student student102 = new Student("regTest", 5);
		System.out.println("RegistrationSystem:   register is working is " + (sys210.register(student102, course101) == false));
		Course course102 = new Course("regTest", 5);
		System.out.println("RegistrationSystem:   register is working is " + (sys210.register(student101, course102) == false));


		//=========== test RegistrationSystem findExamConflicts =========== 
		System.out.println("");

		RegistrationSystem sys220 = new RegistrationSystem(); 
		Course[] courseList = new Course[10];
		Student[] studentList = new Student[10];
		for (int id = 0 ; id < 10 ; id = id+1){
			courseList[id] = new Course("someName", id+1);
			studentList[id] = new Student("someName", id+1);
			sys220.addCourse(courseList[id]);
			sys220.addStudent(studentList[id]);
		}

		for (int stuId = 0; stuId<studentList.length ; stuId= stuId+1){
			int temp = stuId;
			// this part just registers students to some courses. 
			while(temp<courseList.length){
				sys220.register(studentList[stuId], courseList[temp]);
				temp = temp*2+1;

			}

		}

		boolean [][] conflict = sys220.findExamConflicts();
		for (int i = 0; i< conflict.length; i++){
			System.out.println(Arrays.toString(conflict[i]));
		}
		/*
		 * 
	         should print: 
				[false, true, false, true, false, false, false, true, false, false]
				[true, false, false, true, false, false, false, true, false, false]
				[false, false, false, false, false, true, false, false, false, false]
				[true, true, false, false, false, false, false, true, false, false]
				[false, false, false, false, false, false, false, false, false, true]
				[false, false, true, false, false, false, false, false, false, false]
				[false, false, false, false, false, false, false, false, false, false]
				[true, true, false, true, false, false, false, false, false, false]
				[false, false, false, false, false, false, false, false, false, false]
				[false, false, false, false, true, false, false, false, false, false]

		 */
	}

}
