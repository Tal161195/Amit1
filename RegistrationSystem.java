


public class RegistrationSystem {
	private static int MAX_COURSE=500;												//max number of courses 
	private static int MAX_STUDENT=500;												//max number of students 
	private Course courses[]=new Course [MAX_COURSE];								//courses list
	private Student students[]=new Student [MAX_STUDENT];							//students list
	private int numStudents;														//number of students 
	private int numCourses;															//number of courses
	
	public RegistrationSystem(){

	}

	public boolean addStudent(Student student){
		boolean add=true;
		if(numStudents>500)															//Checks if  numStudents is in the range
			add=false;
		else{
			int index=0;															
			while(index<MAX_STUDENT&&add==true&&this.students[index]!=null)			//finds the first empty spot in the array
				index=index+1;
			for(int i=0;i<index&&add==true;i=i+1)									
			{
				if(student.isEqualTo(this.students[i]))								//checks if the id number is already in the array
					add=false;
			}
			if(!add){
				this.students[index]=student;										//adds new student to the array
				numStudents=numStudents+1;											//updates the number of students
			}
		}
		return add;
	}


	public boolean addCourse(Course course){
		boolean add=true;
		if(numCourses>500)														//Checks if  numCourses is in the range
			add=false;
		else{
			int index=0;
			while(index<MAX_COURSE&&add==true&&this.courses[index]!=null)		//finds the first empty spot in the array
				index=index+1;
			for(int i=0;i<index&&add==true;i=i+1)
			{
				if(course.isEqualTo(this.courses[i]))							//checks if the course is already in the array
					add=false;
			}
			if(!add){
				this.courses[index]=course;										//adds new student to the array
				numCourses=numCourses+1;										//Updates the number of courses
			}
		}
		return add;
	}


	public boolean register(Student student, Course course){
		boolean reg=false;
		for(int i=0;i<numCourses&&!reg;i=i+1)									
		{
			if(this.courses[i].getCourseNumber()==course.getCourseNumber()){				//Checks if the course already exists
				for(int j=0;j<numStudents&&!reg;j=j+1)							
					if(this.students[i].getID()==student.getID()){							//Checks if the student already exists
						reg= student.registerTo(course);	
					}
			}
		}
		return reg;
	}




	public boolean [][] findExamConflicts(){
		boolean [][] examconflict=new boolean [numCourses][numCourses];
		for(int i=0;i<examconflict.length;i=i+1)
			for(int j=0;j<examconflict.length;j=j+1)
				examconflict[i][j]=false;												//strats array as false
		for(int i=0;i<examconflict.length;i=i+1)
			for(int j=i+1;j<examconflict.length;j=j+1)
				for(int z=0;z<numStudents;z=z+1)
				{
					if(students[z].registerTo(courses[i])&&students[z].registerTo(courses[j])){		//Checks if conflicts
						examconflict[i][j]=true;
						examconflict[j][i]=true;
					}	
				}
		return examconflict;
	}

}
